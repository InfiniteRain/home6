import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        System.out.println(g);

        Graph g2 = g.clone();
        System.out.println(g2);

        Graph g3 = g.reverseStructureOrderClone();
        System.out.println(g3);

        System.out.println(g.equals(g2));
        System.out.println(g.equals(g3));
        System.out.println(g2.equals(g));
        System.out.println(g2.equals(g3));
        System.out.println(g3.equals(g));
        System.out.println(g3.equals(g2));
        System.out.println("done");

        Graph g4 = new Graph("G");
        g4.createRandomSimpleGraph(6, 9);
        System.out.println(g4);

        System.out.println(g4.equals(g));
    }

    /**
     * Task 14 - Construct a deep clone of a given graph.
     *
     * Only extra methods are needed in the Vertex class in order to complete the task: findArc and arcs.
     */
    static class Vertex {

        public String id;
        public Vertex next;
        public Arc first;
        public int info = 0;
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * Finds an arc within the vertex.
         *
         * @param from Name of the vertex from which the arc points.
         * @param to Name of the vertex to which the arc points.
         * @return Arc if found; null otherwise.
         */
        public Arc findArc(String from, String to) {
            Arc a = first;

            while (a != null) {
                if (a.id.equals(from) && a.target.id.equals(to)) {
                    return a;
                }

                a = a.next;
            }

            return null;
        }

        /**
         * @return The number of arcs of the vector.
         */
        public int arcs() {
            int i = 0;
            Arc a = first;

            while (a != null) {
                i++;
                a = a.next;
            }

            return i;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    static class Arc {

        public String id;
        public Vertex target;
        public Arc next;
        public int info = 0;
        // You can add more fields, if needed

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Task 14 - Construct a deep clone of a given graph.
     *
     * Added methods: clone, reverseStructureOrderClone, equals, findVertex and vertices.
     */
    static class Graph {

        public String id;
        public Vertex first;
        public int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Constructs a deep clone of a graph by iterating through the vertices and the subsequent arcs in a
         * reverse order and cloning the data of the each individual object.
         *
         * @return Cloned graph.
         */
        @Override
        public Graph clone() {
            // The cloned graph.
            Graph clonedGraph = new Graph(id);
            clonedGraph.info = info;

            // HashMap of cloned vertices, keys represent ID's, values are the cloned vertices.
            HashMap<String, Vertex> clonedVertices = new HashMap<>();

            // Array list of original vertices in reverse order.
            ArrayList<Vertex> originalVertices = new ArrayList<>();

            // First vertex of the graph, used for iteration.
            Vertex v = first;

            while (v != null) {
                // Adding the vertices to the originalVertices array list in reverse order.
                originalVertices.add(0, v);

                // Progressing the iteration.
                v = v.next;
            }

            // Creating empty vertex clones. All of them have to be present before cloning the arcs.
            for (Vertex originalVertex : originalVertices) {
                Vertex clonedVertex = clonedGraph.createVertex(originalVertex.id);
                clonedVertex.info = originalVertex.info;
                clonedVertices.put(originalVertex.id, clonedVertex);
            }

            // Cloning the arcs of each vertex.
            for (Vertex originalVertex : originalVertices) {
                // List of the arc's vertices in reverse order.
                ArrayList<Arc> originalArcs = new ArrayList<>();

                // First arc of the vertex, used for iteration.
                Arc a = originalVertex.first;

                while (a != null) {
                    // Adding the arc to the originalArcs array list in reverse order.
                    originalArcs.add(0, a);
                    a = a.next;
                }

                // Cloning each arc of the vertex.
                for (Arc originalArc : originalArcs) {
                    Arc clonedArc = clonedGraph.createArc(
                            originalArc.id,
                            clonedVertices.get(originalVertex.id),
                            clonedVertices.get(originalArc.target.id)
                    );
                    clonedArc.info = originalArc.info;
                }
            }

            // Returns the cloned graph.
            return clonedGraph;
        }

        /**
         * Returns a deep clone of the graph. However, it does not iterate through the vertices and the
         * subsequent arcs in a reverse order, and therefore it won't replicate the EXACT ordering
         * of the original. Nevertheless, it will still be a fully legitimate clone since there will be no
         * changes to the actual graph, it will only be different in how the compiler sees it.
         *
         * @return Cloned graph with a reverse inner structure of the object.
         */
        public Graph reverseStructureOrderClone() {
            // The cloned graph.
            Graph clonedGraph = new Graph(id);
            clonedGraph.info = info;

            // HashMap of cloned vertices, keys represent ID's, values are the cloned vertices.
            HashMap<String, Vertex> clonedVertices = new HashMap<>();

            // First vertex of the graph, used for iteration.
            Vertex v = first;

            while (v != null) {
                Vertex clonedVertex = clonedGraph.createVertex(v.id);
                clonedVertex.info = v.info;
                clonedVertices.put(v.id, clonedVertex);

                // Progressing the iteration.
                v = v.next;
            }

            // Resetting the iterator.
            v = first;

            // Cloning the arcs of each vertex.
            while (v != null) {
                // First arc of the vertex, used for iteration.
                Arc a = v.first;

                while (a != null) {
                    // Adding the arc to the originalArcs array list in reverse order.
                    Arc clonedArc = clonedGraph.createArc(
                            a.id,
                            clonedVertices.get(v.id),
                            clonedVertices.get(a.target.id)
                    );
                    clonedArc.info = a.info;

                    a = a.next;
                }

                // Progressing the iteration.
                v = v.next;
            }

            // Returns the cloned graph.
            return clonedGraph;
        }

        /**
         * Compares two graphs. In order for this to return true, the vertices and arcs have to be equal. The
         * exact order in which the vertices/arcs are defined within the objects does not matter.
         *
         * @param graph Graph to compare this with.
         * @return Result of the comparison.
         */
        public boolean equals(Object graph) {
            Graph compareGraph = (Graph) graph;

            // Compare names.
            if (!id.equals(compareGraph.id)) {
                return false;
            }

            // Compare number of vertices.
            if (vertices() != compareGraph.vertices()) {
                return false;
            }

            // Compare vertices.
            Vertex v = first;

            while (v != null) {
                Vertex compareVertex = compareGraph.findVertex(v.id);

                // Check if it exists.
                if (compareVertex == null) {
                    return false;
                }

                // Compare number of arcs.
                if (v.arcs() != compareVertex.arcs()) {
                    return false;
                }

                // Compare arcs.
                Arc a = v.first;

                while (a != null) {
                    Arc compareArc = compareVertex.findArc(a.id, a.target.id);

                    // Check if it exists.
                    if (compareArc == null) {
                        return false;
                    }

                    a = a.next;
                }

                v = v.next;
            }

            return true;
        }

        /**
         * Finds a vertex with a given ID.
         *
         * @param vid ID.
         * @return Found vertex; null if no vertex was found.
         */
        public Vertex findVertex(String vid) {
            Vertex v = first;

            while (v != null) {
                if (v.id.equals(vid)) {
                    return v;
                }

                v = v.next;
            }

            return null;
        }

        /**
         * @return The total number of vertices on the graph.
         */
        public int vertices() {
            int i = 0;
            Vertex v = first;

            while (v != null) {
                i++;
                v = v.next;
            }

            return i;
        }
    }

} 

