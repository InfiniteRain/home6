import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {
   @Test(timeout = 20000)
   public void assertEquals() {
      GraphTask.Vertex ov1 = new GraphTask.Vertex("v1");
      GraphTask.Vertex ov2 = new GraphTask.Vertex("v2");
      GraphTask.Vertex ov3 = new GraphTask.Vertex("v3");
      GraphTask.Arc oa1_3 = new GraphTask.Arc("a1_3");
      GraphTask.Arc oa3_1 = new GraphTask.Arc("a3_1");
      GraphTask.Arc oa2_3 = new GraphTask.Arc("a2_3");
      GraphTask.Arc oa3_2 = new GraphTask.Arc("a3_2");

      oa1_3.target = ov3;
      oa3_1.target = ov1;
      oa3_1.next = oa3_2;
      oa2_3.target = ov3;
      oa3_2.target = ov2;
      ov1.first = oa1_3;
      ov1.next = ov2;
      ov2.first = oa2_3;
      ov2.next = ov3;
      ov3.first = oa3_1;

      GraphTask.Graph o = new GraphTask.Graph("G", ov1);

      GraphTask.Vertex cv1 = new GraphTask.Vertex("v1");
      GraphTask.Vertex cv2 = new GraphTask.Vertex("v2");
      GraphTask.Vertex cv3 = new GraphTask.Vertex("v3");
      GraphTask.Arc ca1_3 = new GraphTask.Arc("a1_3");
      GraphTask.Arc ca3_1 = new GraphTask.Arc("a3_1");
      GraphTask.Arc ca2_3 = new GraphTask.Arc("a2_3");
      GraphTask.Arc ca3_2 = new GraphTask.Arc("a3_2");

      ca1_3.target = cv3;
      ca3_1.target = cv1;
      ca3_1.next = ca3_2;
      ca2_3.target = cv3;
      ca3_2.target = cv2;
      cv1.first = ca1_3;
      cv1.next = cv2;
      cv2.first = ca2_3;
      cv2.next = cv3;
      cv3.first = ca3_1;

      GraphTask.Graph c = new GraphTask.Graph("G", cv1);

      Assert.assertEquals(o, c);

      cv1.next = null;
      cv2.next = cv1;
      cv3.next = cv2;
      c.first = cv3;

      Assert.assertEquals(o, c);
   }

   @Test(timeout=20000)
   public void assertClone() {
      GraphTask.Graph g1 = new GraphTask.Graph("G");
      g1.createRandomSimpleGraph(6, 9);
      GraphTask.Graph g1c = g1.clone();

      Assert.assertEquals(g1, g1c);

      GraphTask.Graph g2 = new GraphTask.Graph("G");
      g2.createRandomSimpleGraph(6, 9);
      GraphTask.Graph g2c = g2.clone();

      Assert.assertEquals(g2, g2c);
      Assert.assertNotEquals(g1, g2);
   }

   @Test(timeout=20000)
   public void assertInverseEquals() {
      GraphTask.Graph g1 = new GraphTask.Graph("G");
      GraphTask.Graph g1c = g1.clone();
      GraphTask.Graph g1ci = g1.reverseStructureOrderClone();

      Assert.assertEquals(g1, g1c);
      Assert.assertEquals(g1, g1ci);
      Assert.assertEquals(g1c, g1);
      Assert.assertEquals(g1c, g1ci);
      Assert.assertEquals(g1ci, g1);
      Assert.assertEquals(g1ci, g1c);
   }

   @Test(timeout=20000)
   public void assertDeepEquivalence() {
      GraphTask.Graph g1 = new GraphTask.Graph("G");
      g1.createRandomSimpleGraph(6, 9);

      GraphTask.Graph g2 = g1.clone();
      assertNotSame(g1, g2);

      GraphTask.Vertex v1 = g1.first;
      GraphTask.Vertex v2 = g2.first;

      while (v1 != null) {
         assertNotSame(v1, v2);

         GraphTask.Arc a1 = v1.first;
         GraphTask.Arc a2 = v2.first;

         while (a1 != null) {
            assertNotSame(a1, a2);

            a1 = a1.next;
            a2 = a2.next;
         }

         v1 = v1.next;
         v2 = v2.next;
      }
   }

   @Test(timeout = 20000)
   public void assertLargeClone() {
      GraphTask.Graph g1 = new GraphTask.Graph("G");
      g1.createRandomSimpleGraph(2500, 3000);
      GraphTask.Graph g1c = g1.clone();
      GraphTask.Graph g1ci = g1.reverseStructureOrderClone();

      Assert.assertEquals(g1, g1c);
      Assert.assertEquals(g1, g1ci);
      Assert.assertEquals(g1c, g1);
      Assert.assertEquals(g1c, g1ci);
      Assert.assertEquals(g1ci, g1);
      Assert.assertEquals(g1ci, g1c);
   }
}

